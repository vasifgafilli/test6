public class Main {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = DayOfWeek.SUNDAY.name();
        schedule[0][1] = "Work day";
        schedule[1][0] = DayOfWeek.MONDAY.name();
        schedule[1][1] = "Music day";
        schedule[2][0] = DayOfWeek.TUESDAY.name();
        schedule[2][1] = "Football day";
        schedule[3][0] = DayOfWeek.WEDNESDAY.name();
        schedule[3][1] = "Freelance day";
        schedule[4][0] = DayOfWeek.TUESDAY.name();
        schedule[4][1] = "Game day";
        schedule[5][0] = DayOfWeek.FRIDAY.name();
        schedule[5][1] = "Rest day";
        schedule[6][0] = DayOfWeek.SATURDAY.name();
        schedule[6][1] = "Tennis day";
        Human child = new Human("Xavi", "Hernandes", 1979, 140, schedule);
        Human child2 = new Human("Xabi", "Alonso", 1985, 100, schedule);
        Human child3 = new Human("Sergio", "Bustges", 1997, 120, schedule);
        Human mother = new Human("Iniesta", "Zeynalov", 1983);
        Human father = new Human("Pedro", "Rodriguez", 1992);
        Human child4 = new Human("David", "Villa", 1990);


        Human[] children = {child, child3};
        Family alonso = new Family(mother, father, children);
        alonso.deleteChild(child3);
        alonso.countFamily();


    }
}
