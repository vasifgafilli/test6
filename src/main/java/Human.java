import java.util.Arrays;


public class Human {

    private String name;
    private String surname;
    private int dateOfBirth;
    private int iqLevel;
    private String[][] schedule;
    private Family family;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(int dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getIqLevel() {
        return iqLevel;
    }

    public void setIqLevel(int iqLevel) {
        this.iqLevel = iqLevel;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Human(String name, String surname, int dateOfBirth, int iqLevel, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.iqLevel = iqLevel;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int dateOfBirth) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
    }

    public Human() {
    }



    public void greetPet() {
        System.out.printf("\nHello,%s", family.getPet().getNickname());
    }

    public void describePet() {
        String slyness;

        if (family.getPet().getTricklevel() >= 50) {
            slyness = "It's very sly";
        } else {
            slyness = "It's almost not sly";
        }

        System.out.printf("\nit's %d years old,it %s", family.getPet().getAge(), slyness);
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", iqLevel=" + iqLevel +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';

    }


}