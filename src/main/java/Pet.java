import java.util.Arrays;



public class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int tricklevel;
    private String[] habits;


    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTricklevel() {
        return tricklevel;
    }

    public void setTricklevel(int tricklevel) {
        this.tricklevel = tricklevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }


    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.tricklevel = trickLevel;
        this.habits = habits;
    }


    public Pet() {
    }
    public void eat() {
        System.out.println("I am eating");
    }

    public void respond() {
        System.out.println("Hello,my name is " + getNickname() + "I missed you");
    }

    @Override
    public String toString() {
        return "Pet{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + tricklevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
}
